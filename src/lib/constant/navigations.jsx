import {
	HiOutlineQuestionMarkCircle,
	HiOutlineCog,
    
} from 'react-icons/hi'
import { FaShoppingCart } from "react-icons/fa";
import { CiBarcode } from "react-icons/ci";

export const DASHBOARD_SIDEBAR_LINKS = [
	{
        id:1,
		key: 'products',
		label: 'Products',
		path: '/',
		icon: <FaShoppingCart />
	},
    {
        id:2,
		key: 'barcode',
		label: 'Barcode',
        path: '/barcode',
        icon: <CiBarcode />,
	},
]

export const DASHBOARD_SIDEBAR_BOTTOM_LINKS = [
	{
		key: 'settings',
		label: 'Settings',
		path: '/settings',
		icon: <HiOutlineCog />
	},
	{
		key: 'support',
		label: 'Help & Support',
		path: '/support',
		icon: <HiOutlineQuestionMarkCircle />
	}
]