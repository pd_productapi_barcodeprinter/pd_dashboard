import { GrEdit, GrTrash } from 'react-icons/gr'

export const COLUMN_PRODUCTS = (openEditModal) => [
  { field: 'productId', headerName: 'Product ID', flex: 1 },
  { field: 'productCode', headerName: 'Product Code', flex: 2 },
  { field: 'description', headerName: 'Description', flex: 4, renderCell: ({ value }) => <div className="whitespace-normal break-words">{value}</div> },
  {
    field: 'sell',
    headerName: 'Price (Tax Inclusive)',
    flex: 2,
    renderCell: ({ row }) => {
      return <span>Php {row.sell}</span>;
    },
  },
  {
    headerName: 'Actions',
    flex: 2,
    renderCell: ({ row }) => {
      return (
        <div className="flex items-center gap-x-2">
          <GrEdit onClick={() => openEditModal(row)} />
          <GrTrash />
        </div>
      );
    },
  },
];

export const COLUMN_BARCODE = (handleRemoveProduct, handleAddQuantity, handleSubtractQuantity, handleVisibilityToggle) => [
  { field: 'productId', headerName: 'ID', flex: 1, minWidth: 100 },
  { field: 'productCode', headerName: 'Product Code', flex: 2, minWidth: 150 },
  { field: 'description', headerName: 'Description', flex: 3, minWidth: 200, renderCell: ({ value }) => <div className="whitespace-normal break-words">{value}</div>  },
  {
    field: 'sell',
    headerName: 'Price (Tax Inclusive)',
    flex: 2,
    minWidth: 150,
    renderCell: ({ row }) => {
      return <span>Php {row.sell}</span>;
    },
  },
  {
    field: 'visible',
    headerName: 'Visible',
    flex: 1,
    minWidth: 100,
    renderCell: ({ row }) => {
      return (
        <div className="flex justify-center items-center">
          <input type="checkbox" checked={row.visible} onChange={() => handleVisibilityToggle(row.productId)}/>
        </div>
      );
    },
  },
  {
    field: 'qty',
    headerName: 'Quantity',
    flex: 1,
    minWidth: 100,
    renderCell: ({ row }) => {
      return (
        <div className="flex items-center gap-x-2">
          <button onClick={() => handleSubtractQuantity(row.productId)}>-</button>
          <span>{row.quantity}</span>
          <button onClick={() => handleAddQuantity(row.productId)}>+</button>
        </div>
      );
    },
  },
  {
    headerName: 'Actions',
    flex: 2,
    minWidth: 150,
    renderCell: ({ row }) => {
      return row.productId ? (
        <div className="flex items-center gap-x-2">
          <GrTrash onClick={() => handleRemoveProduct(row.productId)} />
        </div>
      ) : null;
    },
  },
];