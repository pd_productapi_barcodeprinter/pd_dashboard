import React, {useEffect} from 'react';
import JsBarcode from 'jsbarcode';

const BarcodeWithInfo = ({sell, value}) => {
    useEffect(() => {
        const barcodeValue = value || '123456789';
        const canvas = document.getElementById('barcode');
    
        JsBarcode(canvas, barcodeValue, {
          format: 'CODE128',
          displayValue: false,
        });
        const containerWidth = canvas.parentElement.clientWidth;
        const maxWidth = 300;
        const adjustedWidth = Math.min(containerWidth, maxWidth);
        canvas.style.width = `${adjustedWidth}px`;
      }, [value]);
  return (
    <div className="mt-4 border border-black p-4 bg-white">
      <div>{value}</div>
      <div className="barcode-container mt-4">
        <canvas id="barcode"></canvas>
      </div>
      <div className="text-right">Php {sell}</div>
    </div>
  )
}

export default BarcodeWithInfo