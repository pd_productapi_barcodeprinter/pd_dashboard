import JsBarcode from 'jsbarcode';
import jsPDF from 'jspdf';

const PrintBarcodes = (printProducts) => {
    const printWindow = new jsPDF({
        unit: 'cm',
        format: [5, 3],
    });
    printProducts.forEach((product, index) => {
        for (let i = 0; i < product.quantity; i++) {
          if (index > 0 || i > 0) {
            printWindow.addPage();
          }
          printWindow.setLineWidth(0.01);
          printWindow.setDrawColor(0);
          printWindow.setFontSize(6);
    
          const availableWidth = printWindow.internal.pageSize.width;
          const availableHeight = printWindow.internal.pageSize.height - 3;
    
          const containerWidth = availableWidth;
          const containerHeight = availableHeight;
    
          const startX = 0;
          const startY = 0;
    
          const barcodeWidth = containerWidth;
          const barcodeHeight = containerHeight - 1;
    
          const barcodeData = product.productCode || '123456789';
          const barcodeOptions = {
            format: 'CODE128',
            width: barcodeWidth - 1,
            height: 50,
            displayValue: false,
          };
    
          const canvas = document.createElement('canvas');
          JsBarcode(canvas, barcodeData, barcodeOptions);
    
          const imgData = canvas.toDataURL('image/png');
          printWindow.addImage(imgData, 'PNG', startX, startY + 0.3, barcodeWidth, barcodeHeight);
    
          const productCodeY = startY + 0.3;
          const priceTextX = startX + 2.1;
          const priceTextY = startY + 1.4;
    
          printWindow.text(product.productCode, startX, productCodeY, { fontSize: 3 });
          if(product.visible){
            printWindow.text('Php ' + product.sell, priceTextX, priceTextY, { fontSize: 8 });
          }
        }
      });


    printWindow.autoPrint();
    printWindow.output('dataurlnewwindow');
}

export default PrintBarcodes;