import React, { useState, useEffect } from 'react';

const EditProductModal = ({ isOpen, onClose, product, onSave }) => {
  const [editedProduct, setEditedProduct] = useState({ ...product });

  useEffect(() => {
    setEditedProduct({ ...product });
  }, [product]);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    if (name === 'sell') {
      const isValidNumber = value === '' || /^[0-9]+(\.[0-9]*)?$/.test(value);
  
      if (!isValidNumber) {
        return;
      }
    }
  
    setEditedProduct((prevProduct) => ({
      ...prevProduct,
      [name]: value,
    }));
  };

  const handleSave = () => {
    if (editedProduct.description === '' || editedProduct.sell === '') {
      window.alert('Properties cannot be empty');
      return;
    }

    const editedProductWithDefaults = {
      ...editedProduct,
      sell: editedProduct.sell === '' ? 0 : editedProduct.sell,
    };

    onSave(editedProductWithDefaults);
    onClose();
  };

  return (
    <div className={`fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 ${isOpen ? 'block' : 'hidden'}`}>
      <div className="bg-white p-6 border border-gray-300 rounded">
        <h2 className="text-xl font-bold mb-4">Edit Product</h2>
        <div className="mb-4">
          <label className="block text-sm mb-1">Product Code:</label>
          <span className="text-gray-600">{product.productCode}</span>
        </div>
        <label className="block mb-4">
        Description:
          <textarea
            name="description"
            value={editedProduct.description}
            onChange={handleInputChange}
            className="w-full border border-gray-300 p-2 rounded"
            rows="3"
          />
        </label>

        <label className="block mb-4">
          Sell Price:
          <input
            type="text"
            name="sell"
            value={editedProduct.sell}
            onChange={handleInputChange}
            className="w-full border border-gray-300 p-2 rounded"
          />
        </label>
        <div className="flex justify-end">
          <button
            onClick={handleSave}
            className="bg-blue-500 text-white px-4 py-2 mr-2 rounded hover:bg-blue-600"
          >
            Save
          </button>
          <button
            onClick={onClose}
            className="bg-gray-300 text-gray-700 px-4 py-2 rounded hover:bg-gray-400"
          >
            Cancel
          </button>
        </div>
      </div>
    </div>
  );
}

export default EditProductModal;