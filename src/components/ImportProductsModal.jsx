import React, { useState } from 'react';
import axiosConfig from '../api/axiosConfig';


const ImportProductsModal = ({ isOpen, onClose, onImport }) => {
  let api = axiosConfig();
  const [selectedFile, setSelectedFile] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    setSelectedFile(file);
  };

  const handleImport = async () => {
    if (selectedFile) {
      const formData = new FormData();
      formData.append('file', selectedFile);
      try { // Set loading to true before making the API call
        setIsLoading(true);
        const response = await api.uploadFile(formData);
        if (response.status === 200) {
          setIsLoading(false);
          onImport(response);
  
          onClose();
        }
      } catch (error) {
        setIsLoading(false);
        onImport(error);
        onClose();
        console.error('Error uploading file:', error.message);
      }
    }
  };

  return (
    <div className={`fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 ${isOpen ? 'block' : 'hidden'}`}>
      <div className="bg-white p-6 border border-gray-300 rounded">
        <h2 className="text-xl font-bold mb-4">Import CSV</h2>
        <label className="block mb-4">
          Select a CSV file:
          <input
            type="file"
            accept=".csv"
            onChange={handleFileChange}
            className="w-full border border-gray-300 p-2 rounded"
          />
        </label>
        <div className="flex justify-end">
          <button
            type="submit"
            onClick={handleImport}
            className={`bg-blue-500 text-white px-4 py-2 mr-2 rounded hover:bg-blue-600 ${isLoading ? 'opacity-50 cursor-not-allowed' : ''}`}
            disabled={isLoading}
          >
            {isLoading ? (
              <div className="animate-spin rounded-full h-6 w-6 border-t-2 border-b-2 border-blue-700"></div>
            ) : (
              'Import'
            )}
          </button>
          <button
            onClick={onClose}
            className={`bg-gray-300 text-gray-700 px-4 py-2 rounded hover:bg-gray-400 ${isLoading ? 'opacity-50 cursor-not-allowed' : ''}`}
            disabled={isLoading}
          >
            Cancel
          </button>
        </div>
      </div>
    </div>
  );
};

export default ImportProductsModal