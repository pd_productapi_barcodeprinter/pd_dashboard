import React, {useState} from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import logo from '../../assets/pdlogo.jpeg';
import { DASHBOARD_SIDEBAR_LINKS} from '../../lib/constant/navigations';
import classnames from 'classnames';
import { HiMenu } from 'react-icons/hi'; 

const linkClass ='flex items-center gap-2 font-light px-3 py-2 hover:bg-neutral-700 hover:no-underline active:bg-neutral-600 rounded-sm text-base'
const Sidebar = () => {
  const navigate = useNavigate();
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  return (
    <div className='flex flex-col bg-neutral-900 w-full md:w-60 p-3 text-white'>
      <div className="flex items-center justify-between gap-2 px-1 py-3 font-bold cursor-pointer">
        <div className={`flex items-center`}>
        <div className="hidden md:flex items-center" onClick={() => navigate('/')}>
          <img src={logo} className="h-10 w-10" style={{ borderRadius: "50%" }} alt="logo" />
          <span className="text-neutral-100 text-lg ml-2">Pacifica Dive</span>
        </div>
        </div>
        <HiMenu className="md:hidden cursor-pointer" onClick={() => setIsMenuOpen(!isMenuOpen)} />
      </div>
      <div className={`flex-1 flex flex-col gap-0.5 ${isMenuOpen ? 'block' : 'hidden'} md:flex md:flex-col`}>
        {DASHBOARD_SIDEBAR_LINKS.map((link) => (
          <SidebarLink key={link.key} link={link} />
        ))}
      </div>
    </div>
  )
}

export default Sidebar

function SidebarLink({link}){
    const { pathname } = useLocation();

    return (
        <Link to={link.path} className={classnames(pathname === link.path ? 'text-white':'text-neutral-400',linkClass)}>
            <span className='text-xl'>{link.icon}</span>
            <span className='ml-2'>
              {link.label}
            </span>
        </Link>
    )
}