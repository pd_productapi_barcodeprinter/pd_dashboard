import React, {useState, useEffect} from 'react';
import { MdOutlineLocalPrintshop } from "react-icons/md";
import { HiOutlineSearch } from 'react-icons/hi';
import BarcodeWithInfo from '../components/BarcodeWithInfo';
import { DataGrid } from '@mui/x-data-grid';
import axiosConfig from '../api/axiosConfig';
import PrintBarcodes from '../components/PrintBarcodes';
import { COLUMN_BARCODE } from '../lib/constant/columns';

const Barcode = () => {
  const [printProducts, setPrintProducts] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [products, setProducts] = useState([]);
  const [searchInput, setSearchInput] = useState('');
  const [suggestedProducts, setSuggestedProducts] = useState([]);

  const handleSearchKeyPress = (e) => {
    if (e.key === 'Enter') {
      const filteredProducts = products?.filter((item) =>
        Object.values(item).some(
          (value) =>
            value && value.toString().toLowerCase().includes(searchInput.toLowerCase())
        )
      );

      if (filteredProducts.length > 0) {
        const existingProduct = printProducts.find(
          (product) => product.productId === filteredProducts[0].productId
        );

        if (existingProduct) {
          setPrintProducts((prevPrintProducts) =>
            prevPrintProducts.map((product) =>
              product.productId === existingProduct.productId
                ? { ...product, quantity: product.quantity + 1 }
                : product
            )
          );
        } else {
          setPrintProducts((prevPrintProducts) => [
            ...prevPrintProducts,
            { ...filteredProducts[0], quantity: 1, visible: true },
          ]);
        }

        setSelectedProduct(filteredProducts[0]);
      }

      setSearchInput('');
      setSuggestedProducts([]);
    } else {
      const suggestions = products?.filter((item) =>
        Object.values(item).some(
          (value) =>
            value &&
            value.toString().toLowerCase().includes(searchInput.toLowerCase())
        )
      );
      setSuggestedProducts(suggestions);
    }
  };

  const handleSuggestionClick = (suggestion) => {
    const existingProduct = printProducts.find(
      (product) => product.productId === suggestion.productId
    );

    if (existingProduct) {
      setPrintProducts((prevPrintProducts) =>
        prevPrintProducts.map((product) =>
          product.productId === existingProduct.productId
            ? { ...product, quantity: product.quantity + 1 }
            : product
        )
      );
    } else {
      setPrintProducts((prevPrintProducts) => [
        ...prevPrintProducts,
        { ...suggestion, quantity: 1, visible: true },
      ]);
    }

    setSelectedProduct(suggestion);
    setSearchInput('');
    setSuggestedProducts([]);
  };

  const handleRemoveProduct = (productIdToRemove) => {
    setPrintProducts((prevPrintProducts) =>
      prevPrintProducts.filter(
        (product) => product.productId !== productIdToRemove
      )
    );
  };

  const handleAddQuantity = (productId) => {
    setPrintProducts((prevPrintProducts) =>
      prevPrintProducts.map((product) =>
        product.productId === productId
          ? { ...product, quantity: product.quantity + 1 }
          : product
      )
    );
  };
  
  const handleSubtractQuantity = (productId) => {
    setPrintProducts((prevPrintProducts) =>
      prevPrintProducts.map((product) =>
        product.productId === productId && product.quantity > 1
          ? { ...product, quantity: product.quantity - 1 }
          : product
      )
    );
  };

  const handleVisibilityToggle = (productId) => {
    setPrintProducts((prevPrintProducts) =>
      prevPrintProducts.map((product) =>
        product.productId === productId
          ? { ...product, visible: !product.visible }
          : product
      )
    );
  };

  useEffect(() => {
    let api = axiosConfig();
    const getProducts = async () => {
      try {
        const productsData = await api.getProducts();
        setProducts(productsData);
      } catch (error) {
        console.error('Error fetching products:', error);
      }
    };
    getProducts();
  }, []);

  return (
    <div>
      <div className='bg-white h-16 px-4 flex justify-between items-center border-b border-gray-200'>
        <div className='relative'>
          <HiOutlineSearch fontSize={20} className='text-gray-400 absolute top-1/2 -translate-y-1/2 left-3'/>
          <input
            type='text'
            placeholder='Search Product'
            value={searchInput}
            onChange={(e) => setSearchInput(e.target.value)}
            onKeyDown={handleSearchKeyPress}
            onBlur={() => {
              if (searchInput.trim() === '') {
                setSuggestedProducts([]);
              }
            }}
            className='text-sm focus:outline-none active:outline-none h-10 pl-10 w-[24rem] border border-gray-300 rounded-sm px-4'
          />
          {suggestedProducts.length > 0 && (
            <ul
              className="absolute top-11 left-0 right-0 bg-white border border-gray-300 rounded-sm shadow mt-1"
              style={{ zIndex: 1 }}
            >
              {suggestedProducts.map((suggestion) => (
                <li
                  key={suggestion.productId}
                  onClick={() => handleSuggestionClick(suggestion)}
                  className="cursor-pointer py-2 px-4 hover:bg-gray-100"
                >
                  {suggestion.productCode}
                </li>
              ))}
            </ul>
          )}
        </div>
        <div className='flex items-center gap-2 mr-2'>
          {printProducts.length > 0 ? (
            <MdOutlineLocalPrintshop
              fontSize={20}
              onClick={() => PrintBarcodes(printProducts)}
              style={{ cursor: 'pointer' }}
              title="Print Labels"
            />
          ) : (
            <span
              onClick={() => alert('Please add products to print')}
              style={{ cursor: 'pointer', color: 'gray' }}
            >
              <MdOutlineLocalPrintshop fontSize={20} />
            </span>
          )}
        </div>
      </div>
      
      <div className='flex flex-col md:flex-row justify-center items-center py-5'>
        <div className='w-full px-3 md:w-1/2 lg:w-2/3 xl:w-3/4 overflow-auto' style={{ height: window.innerHeight-100}}>
          <DataGrid
            rows={printProducts}
            columns={COLUMN_BARCODE(handleRemoveProduct, handleAddQuantity, handleSubtractQuantity, handleVisibilityToggle)}
            getRowId={(row) => row.productId}
            onRowClick={(params) => {
              const selectedProductId = params.row.productId;
              const selectedProduct = printProducts.find((product) => product.productId === selectedProductId);
              setSelectedProduct(selectedProduct || null);
            }}
          />
        </div>
        <div className='flex items-center md:w-1/2 lg:w-1/3 xl:w-1/4'>
          <div className="pl-2 w-[300px]">
            <h1>Product Information</h1>
            <BarcodeWithInfo
              sell={selectedProduct?.sell}
              value={selectedProduct?.productCode}
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Barcode