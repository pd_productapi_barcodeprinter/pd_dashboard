import React, {useState, useEffect} from 'react'
import { DataGrid } from '@mui/x-data-grid';
import { HiOutlineSearch } from 'react-icons/hi';
import { LuImport } from "react-icons/lu";
import { PiExportBold } from "react-icons/pi";
import axiosConfig from '../api/axiosConfig';
import { COLUMN_PRODUCTS } from '../lib/constant/columns';
import EditProductModal from '../components/EditProductModal';
import ImportProductsModal from '../components/ImportProductsModal';


const Products = () => {
  let api = axiosConfig();
  const [searchTerm, setSearchTerm] = useState('');
  const [products, setProducts] = useState([]);
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [selectedProductForEdit, setSelectedProductForEdit] = useState(null);
  const [isImportModalOpen, setIsImportModalOpen] = useState(false);

  const openEditModal = (product) => {
    setSelectedProductForEdit(product);
    setIsEditModalOpen(true);
  };
  
  const closeEditModal = () => {
    setIsEditModalOpen(false);
  };

  const openImportModal = () => {
    setIsImportModalOpen(true);
  };

  const closeImportModal = () => {
    setIsImportModalOpen(false);
  };
  
  const handleSaveEdit = async (editedProduct) => {
    try{
      const response = await api.updateProduct(editedProduct);
      if (response === 'Success') {
        setProducts((prevProducts) => {
          const index = prevProducts.findIndex(
            (product) => product.productId === editedProduct.productId
          );
          const updatedProducts = [...prevProducts];
          updatedProducts[index] = editedProduct;
          return updatedProducts;
        });
  
        console.log('Product updated successfully!');
      }
  
    }catch (error){
      console.log('Error updating product: ', error.message);
    }
  };

  const handleImport = async (response) => {
    if (response.status === 200) {
        alert(response.data);
        window.location.reload();
    }else{
      alert(response.response.data.message);
    }
  };

  useEffect(() => {
    let api = axiosConfig();
    const getProducts = async () => {
      try {
        const productsData = await api.getProducts();
        setProducts(productsData);
      } catch (error) {
        console.error('Error fetching products:', error);
      }
    };

    getProducts();
  }, []);

  const handleSearch = (e) => {
    setSearchTerm(e.target.value);
  };

  const filteredData = products?.filter((item) =>
    Object.values(item).some(
      (value) =>
        value && value.toString().toLowerCase().includes(searchTerm.toLowerCase())
    )
  );

  return (
    <div>
      <div className='bg-white h-16 px-4 flex justify-between items-center border-b border-gray-200'>
        <div className='relative'>
          <HiOutlineSearch fontSize={20} className='text-gray-400 absolute top-1/2 -translate-y-1/2 left-3'/>
          <input type='text' placeholder='Search Product' onChange={handleSearch} value={searchTerm} className='text-sm focus:outline-none active:outline-none h-10 pl-10 w-[24rem] border border-gray-300 rounded-sm px-4'/>
        </div>
        <div className='flex items-center gap-2 mr-2'>
          <LuImport fontSize={20} onClick={openImportModal} title="Import Products"/>
          <PiExportBold fontSize={20} title="Export Products"/>
        </div>
      </div>
      
      <div className="flex flex-col items-center overflow-auto w-full" style={{ height: window.innerHeight-100}}>
        {filteredData && filteredData.length > 0 ? (
          <div className="w-full">
            <DataGrid
              rows={filteredData}
              columns={COLUMN_PRODUCTS(openEditModal)}
              getRowId={(row) => row?.productId}
              autoHeight
              disableExtendRowFullWidth // Optional, if present in your DataGrid version
            />
          </div>
        ) : (
          <p className="mt-4">No products found.</p>
        )}
      </div>
      {isEditModalOpen && (
        <EditProductModal
          isOpen={isEditModalOpen}
          onClose={closeEditModal}
          product={selectedProductForEdit}
          onSave={handleSaveEdit}
        />
      )}
      {isImportModalOpen && (
        <ImportProductsModal
          isOpen={isImportModalOpen}
          onClose={closeImportModal}
          onImport={handleImport}
        />
      )}
    </div>
  )
}

export default Products