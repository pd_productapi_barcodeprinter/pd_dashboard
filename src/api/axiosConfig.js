import axios from "axios";

const BASE_URL = "/api/v1";

const axiosConfig = () => {

    const axiosInstance = axios.create({
        baseURL:BASE_URL,
        headers:{
            "Content-type": "application/json",
            "Access-Control-Allow-Origin": "*",
        }
    });

    const getProducts = async () => {
        try {
          const response = await axiosInstance.get('/products');
          const responseData = Array.from(response.data).sort((a, b) => a.productId - b.productId);
    
          return responseData;
        } catch (error) {
          console.error('Request failed:', error);
        }
    };

    const updateProduct = async (editProduct) => {
        const editedProduct = {
          productCode: editProduct.productCode,
          description: editProduct.description,
          sell: editProduct.sell,
          cost: editProduct.cost,
        };
        try{
          const response = await axiosInstance.put('/products', editedProduct);
          if (response.status === 200) {
            console.log('Product updated successfully:', response.data);
            return response.data;
          } else {
            console.error('Failed to update product:', response.statusText);
            return null;
          }
        }catch(error){
          console.log('Error updating product: ', error.message);
        }
    }

    const uploadFile = async (formData) => {
      try {
        const response = await axiosInstance.post('/products/import', formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        });
    
        // Handle the response from the server
        //console.log('Upload successful:', response.data);
    
        // Return any relevant data from the response
        return response;
      } catch (error) {
        // Handle errors
        console.error('Error uploading file:', error.message);
        throw error; // Re-throw the error to handle it in the calling function
      }
    }; 


    return {axiosInstance,getProducts,updateProduct,uploadFile};
}
export default axiosConfig;