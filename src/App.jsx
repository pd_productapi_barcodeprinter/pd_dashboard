import React from "react";
import {BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Layout from "./components/shared/Layout";
import Products from "./pages/Products";
import Barcode from "./pages/Barcode";

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Products />} />
            <Route path='barcode' element={<Barcode />} />
          </Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
